// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDm338ZZ7pdFmCsEsHWhpVBD1Sr_Y0R2rw",
    authDomain: "hello-mohamad.firebaseapp.com",
    databaseURL: "https://hello-mohamad.firebaseio.com",
    projectId: "hello-mohamad",
    storageBucket: "hello-mohamad.appspot.com",
    messagingSenderId: "24403685182",
    appId: "1:24403685182:web:8dbb0500dec6a46235466f",
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
