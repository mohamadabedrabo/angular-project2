import { AngularFireModule } from '@angular/fire';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreCollectionGroup} from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { title } from 'process';


@Injectable({
  providedIn: 'root'
})
export class BooksService {
  /*
  books = [{title:'Alice in Wonderland', author:'Lewis Carrol', summary:'y default, the expansion-panel header includes a toggle icon at the end of the header to indicate the expansion state. This icon can be hidden via the'},
  {title:'War and Peace', author:'Leo Tolstoy',summary:'y default, the expansion-panel header includes a toggle icon at the end of the header to indicate the expansion state. This icon can be hidden via the'},
  {title:'The Magic Mountain', author:'Thomas Mann',summary:'y default, the expansion-panel header includes a toggle icon at the end of the header to indicate the expansion state. This icon can be hidden via the'}];

  public addBooks() {
    setInterval(()=>this.books.push({title:'A new one',author:'New author',summary:'this is the summary'}),2000);
  }
  
  public getBooks () {
    const booksObservable = new Observable(observer => {
      setInterval(()=>observer.next(this.books),500)
    });
    return booksObservable; 
  }



  public getBooks() {
    return this.books;
  }
  

  */
  bookCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');

  public getBooks(userId){
    this.bookCollection = this.db.collection(`users/${userId}/books`, 
    ref => ref.orderBy('title', 'asc').limit(4)); 
    return this.bookCollection.snapshotChanges()      
  } 

  nextPage(userId,startAfter): Observable<any[]>{
    this.bookCollection = this.db.collection(`users/${userId}/books`, 
    ref => ref.limit(4).orderBy('title', 'asc')
      .startAfter(startAfter))    
    return this.bookCollection.snapshotChanges();
  }
  
  prevPage(userId,startAt): Observable<any[]>{
    this.bookCollection = this.db.collection(`users/${userId}/books`, 
    ref => ref.limit(4).orderBy('title', 'asc')
      .startAt(startAt))    
    return this.bookCollection.snapshotChanges();
  }

  /*public getBooks(userId) {
    this.bookCollection = this.db.collection(`users/${userId}/books`);
    return this.bookCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data;
        }
      )
    ))
  }*/

  deleteBook(Userid:string,id:string) {
    this.db.doc(`users/${Userid}/books/${id}`).delete();
  }

  updateBook(userId:string, id:string, title:string, author:string) {
    this.db.doc(`users/${userId}/books/${id}`).update( 
      {
      title:title,
      author:author
      }
    )
  }

  addBook(userId:string,title:string, author:string) {
    const book = {title:title, author:author};
    this.userCollection.doc(userId).collection('books').add(book);
  }
  
  constructor(private db:AngularFirestore) { }


}
