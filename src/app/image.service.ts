import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  private path:string = 'https://firebasestorage.googleapis.com/v0/b/hello-mohamad.appspot.com/o/';
  public images:string[] = [];
  constructor() {
    this.images[0] = this.path + 'biz.png' + '?alt=media&token=575528b1-0f0c-4f8c-acae-a6aefde0310e';
    this.images[1] = this.path + 'entermnt.png' + '?alt=media&token=6a7746ea-4007-4c86-a820-f1b5bb148770';
    this.images[2] = this.path + 'politics-icon.png' + '?alt=media&token=ee498763-bd8b-42f6-9822-47b9ce4e0cd5';
    this.images[3] = this.path + 'sport.png' + '?alt=media&token=16b2c50a-f29d-4d9d-9ffe-ad20a6bf207b';
    this.images[4] = this.path + 'tech.png' + '?alt=media&token=21746847-acc3-411b-bc32-e764c26bceb5';
   }
}
